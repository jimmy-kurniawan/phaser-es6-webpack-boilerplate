# Your project
#### Your description

# Setup

* Install [nvm](https://github.com/creationix/nvm)
```
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.32.0/install.sh | bash
```

* Install dependencies
```
nvm install && npm install
```

* Run development
```
npm start
```

* Test
```
npm test
```

* Build
```
npm run build
```
